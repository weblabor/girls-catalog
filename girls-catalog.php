<?php
/*
  Plugin Name: Catalogo de modelos
  Description: Guarda y muestra un catalogo de modelos por ciudades
  Author: Carlos Escobar
  Author URI: http://www.weblabor.mx
  Version: 1.0

  Licenced under the GNU GPL:

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// If this file is called directly, then abort execution.
if ( ! defined( 'WPINC' ) ) {
    die;
}

require_once plugin_dir_path( __FILE__ ) . 'includes/class-girls-catalog-manager.php';


function run_girls_catalog_manager() {
    $mct = new Girls_Catalog_Manager();
    $mct->run();
}

run_girls_catalog_manager();

function get_girls($quantity) {
    $r = new WP_Query( array(
       'posts_per_page' => $quantity,
       'no_found_rows' => true, /*suppress found row count*/
       'post_status' => 'publish',
       'post_type' => 'girl',
       'ignore_sticky_posts' => true,
    ) );
    if (!$r->have_posts()) 
        return [];

    $girls = [];
    while ( $r->have_posts() ) : $r->the_post(); 
        $name = get_the_title();
        $measures = get_post_meta(null, 'wgc_measures', true);
        $personality = get_post_meta(null, 'wgc_personality', true);
        $weight = get_post_meta(null, 'wgc_weight', true);
        $url = get_the_post_thumbnail_url(null, 'full');
        $tags = get_the_tags();
        $link = get_permalink();
        $city = null;
        if ($tags) {
            foreach($tags as $tag) {
                $city = $tag->name;
                break;
            }
        }
        $girl = [
            'measures' => $measures,
            'personality' => $personality,
            'weight' => $weight,
            'url' => $url,
            'city' => $city,
            'name' => $name,
            'link' => $link
        ];
        $girls[] = $girl;
    endwhile;
    // Reset the global $the_post as this query will have stomped on it
    wp_reset_postdata();
    return $girls;
}

?>
