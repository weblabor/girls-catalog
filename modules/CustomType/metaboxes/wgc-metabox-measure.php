<?php

/**
 * Class for making the metabox called Year
 *
 * @package WGCM
 */

class WGC_Metabox_Measure implements WGC_metaboxes {
    
    private $id = "measures";
    private $title = "Medidas";
    private $screen = "girl";
    private $context = "normal";
    private $priority = "default";

    public function __construct($loader) {
    	$loader->add_action("admin_init", $this, "create");
    	$loader->add_action('save_post', $this, 'save');
    }

    public function create() {
    	add_meta_box( $this->id, $this->title, array($this, 'display'), $this->screen, $this->context, $this->priority);
    }

    public function display() {
    	global $post;
	  	$field = get_post_meta($post->ID, 'wgc_measures', true);
	  	wp_nonce_field( 'wgc_measures_meta_box_nonce', 'wgc_measures_meta_box_nonce' );

	  	?>
	 		<input type="text" class="widefat" name="measures" value="<?php if($field != '') echo esc_attr( $field ); ?>" />
	   
	  	<?php
    }

    public function save($post_id) {
    	if ( ! isset( $_POST['wgc_measures_meta_box_nonce'] ) ||
	  	! wp_verify_nonce( $_POST['wgc_measures_meta_box_nonce'], 'wgc_measures_meta_box_nonce' ) )
	    	return;
	  
	  	if (!current_user_can('edit_post', $post_id))
	    	return;
	  
	  	$old = get_post_meta($post_id, 'wgc_measures', true);
	  	$new = $_POST['measures'];

		if ( !empty( $new ) && $new != $old )
			update_post_meta( $post_id, 'wgc_measures', $new );
		elseif ( empty($new) && $old )
		    delete_post_meta( $post_id, 'wgc_measures', $old );
    }

}
