<?php

/**
 * Class for making the metabox called Year
 *
 * @package WGCM
 */

class WGC_Metabox_Weight implements WGC_metaboxes {
    
    private $id = "weight";
    private $title = "Edad";
    private $screen = "girl";
    private $context = "normal";
    private $priority = "default";

    public function __construct($loader) {
    	$loader->add_action("admin_init", $this, "create");
    	$loader->add_action('save_post', $this, 'save');
    }

    public function create() {
    	add_meta_box( $this->id, $this->title, array($this, 'display'), $this->screen, $this->context, $this->priority);
    }

    public function display() {
    	global $post;
	  	$field = get_post_meta($post->ID, 'wgc_weight', true);
	  	wp_nonce_field( 'wgc_weight_meta_box_nonce', 'wgc_weight_meta_box_nonce' );

	  	?>
	 		<input type="text" class="widefat" name="weight" value="<?php if($field != '') echo esc_attr( $field ); ?>" />
	   
	  	<?php
    }

    public function save($post_id) {
    	if ( ! isset( $_POST['wgc_weight_meta_box_nonce'] ) ||
	  	! wp_verify_nonce( $_POST['wgc_weight_meta_box_nonce'], 'wgc_weight_meta_box_nonce' ) )
	    	return;
	  
	  	if (!current_user_can('edit_post', $post_id))
	    	return;
	  
	  	$old = get_post_meta($post_id, 'wgc_weight', true);
	  	$new = $_POST['weight'];

		if ( !empty( $new ) && $new != $old )
			update_post_meta( $post_id, 'wgc_weight', $new );
		elseif ( empty($new) && $old )
		    delete_post_meta( $post_id, 'wgc_weight', $old );
    }

}
