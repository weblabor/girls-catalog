<?php

/**
 * Interface for metaboxes
 *
 * @package WGCM
 */

interface WGC_metaboxes {

    public function __construct($loader);
    public function create();
    public function display();
    public function save($post_id);

}
