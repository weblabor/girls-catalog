<?php

/**
 * This class defines all functionality for the dashboard
 * of the plugin
 *
 * @package WGCM
 */

class WGC_Custom_Type {
    
    public function create_post_type() {
        register_post_type( 'girl',
            array(
                'labels' => array(
                    'name' => __( 'Modelos' ),
                    'singular_name' => __( 'Modelo' )
                ),
                'public' => true,
                'has_archive' => true,
                'supports'=> array("title","author","thumbnail"),
                'taxonomies' => ['post_tag']
            )
        );
    }

}
