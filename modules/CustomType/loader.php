<?php

require_once plugin_dir_path( __FILE__ ) . 'class-wgc-custom-type.php';
require_once plugin_dir_path( __FILE__ ) . 'metaboxes/wgc-metaboxes.php';
require_once plugin_dir_path( __FILE__ ) . 'metaboxes/wgc-metabox-measure.php';
require_once plugin_dir_path( __FILE__ ) . 'metaboxes/wgc-metabox-personality.php';
require_once plugin_dir_path( __FILE__ ) . 'metaboxes/wgc-metabox-weight.php';
