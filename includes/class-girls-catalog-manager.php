<?php
/**
 * The Movies Custom Type Manager is the core plugin responsible for including and
 * instantiating all of the code that composes the plugin
 *
 * @package    WGCM
 */

class Girls_Catalog_Manager {

    protected $loader;

    public function __construct() {
        $this->load_dependencies();
        $this->define_admin_hooks();
        $this->load_metaboxes();
    }
   
    private function load_dependencies() {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'modules/loader.php'; // Load modules classes
        require_once plugin_dir_path( __FILE__ ) . 'class-girls-catalog-manager-loader.php';
        $this->loader = new Girls_Catalog_Manager_Loader();
    }
    
    private function define_admin_hooks() {
        $customType = new WGC_Custom_Type();
        $this->loader->add_action( 'init', $customType, 'create_post_type' );
    }

    private function load_metaboxes() {
        new WGC_Metabox_Measure($this->loader);
        new WGC_Metabox_Personality($this->loader);
        new WGC_Metabox_Weight($this->loader);
    }
    
    public function run() {
        $this->loader->run();
    }

}